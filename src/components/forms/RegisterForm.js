import React, { useState } from 'react';


const API_REGISTER_URL = 'https://survey-poodle.herokuapp.com/v1/api/users/register';

const RegisterForm = props => {


    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');


    const onRegisterClicked = async ev => {
        console.log('EVENT: ', ev.target);

        const fetchOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                user: {
                    username,
                    password
                }
            })
        }

        try {
            const result = await fetch(API_REGISTER_URL, fetchOptions).then(r => r.json())
            console.log(result);

        } catch (e) {
            console.log(e)
        } finally {

            //Done
            props.click({
                success: true,
                message: 'Register successfully'
            });

        }

    }

    const onUsernameChange = ev => setUsername(ev.target.value);
    const onPasswordChange = ev => setPassword(ev.target.value);

    return (
        <form>
            <div>
                <label>Username: </label>
                <input type="text" placeholder="Enter a username" onChange={onUsernameChange} ></input>
            </div>
            <div>
                <label>Password: </label>
                <input type="password" placeholder="Enter a password" onChange={onPasswordChange} ></input>
            </div>

            <div>
                <button type="button" onClick={onRegisterClicked}>Register</button>
            </div>
        </form>
    )
}

export default RegisterForm;
