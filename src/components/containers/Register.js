import React from 'react';
import RegisterForm from '../forms/RegisterForm';

const Register = () => {

    const handleRegisterClicked = (result) => {
        console.log('Triggered from RegisterForm', result);
    };

    return (

        <div>
            <h1>Register for Survey</h1>

            <RegisterForm click={ handleRegisterClicked }></RegisterForm>
        </div>

    );

};

export default Register;